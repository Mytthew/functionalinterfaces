package com.company;

import java.util.function.*;

public class FunctionalInterfaces {

	public static void main(String[] args) {
		BiFunction<Integer, Integer, Integer> sum = new BiFunction<Integer, Integer, Integer>() {
			@Override
			public Integer apply(Integer integer, Integer integer2) {
				return null;
			}
		};
		IntPredicate ifPrime = new IntPredicate() {
			@Override
			public boolean test(int value) {
				return false;
			}
		};
		Supplier nextPrime = new Supplier() {
			@Override
			public Object get() {
				return null;
			}
		};
		System.out.println("2 + 2 = " + sum.apply(2, 2));
		System.out.println("Liczba 3 jest " + (ifPrime.test(3) ? "pierwsza" : "niepierwsza"));
		System.out.println("Liczba 4 jest " + (ifPrime.test(4) ? "pierwsza" : "niepierwsza"));
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
		System.out.println("Kolejną liczbą pierwszą jest: " + nextPrime.get());
    }
}
